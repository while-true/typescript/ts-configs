import { ConfigOptions, Config } from 'karma';

const debug = process.argv.includes('--debug');
const noCoverage = process.argv.includes('--no-coverage');
if (debug) {
  // tslint:disable-next-line: no-console
  console.warn('Debug mode is enabled');
}

/**
 * This is a base karma config factory for cross platform typescript projects.
 */
export const karmaConfigFactory: (config: Config) => ConfigOptions = (config: Config) => {
  return {

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha', 'chai'],

    // list of files / patterns to load in the browser
    files: [
      './src/lib/tests/index.test.ts',
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      './src/lib/tests/index.test.ts': ['webpack', 'sourcemap'],
    },

    webpack: {
      node: { fs: 'empty' },
      devtool: 'inline-source-map',
      mode: 'development',
      resolve: {
        extensions: ['.js', '.ts', '.json'],
      },
      module: {
        rules: [
          {
            include: /\.ts$/,
            exclude: /\.test\.ts$/,
            use: [...(debug || noCoverage ? [] : ['@jsdevtools/coverage-istanbul-loader']), 'ts-loader'],
          },
        ],
      },
      output: {
        // Outputs absolute file paths instead of webpack:///path/to/file.extension
        // This makes stacktrace paths clickable in a shell (e.g. VS Code)
        // tslint:disable-next-line: completed-docs
        devtoolModuleFilenameTemplate(info: { absoluteResourcePath: string; }) {
          return info.absoluteResourcePath;
        },
      },
    },
    webpackMiddleware: { stats: 'errors-only' },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: [
      ...(debug || noCoverage ? [] : ['coverage-istanbul']),
      'mocha',
    ],

    coverageIstanbulReporter: {
      dir: './coverage/%browser%',
      // reports can be any that are listed here:
      // https://github.com/istanbuljs/istanbuljs/tree/73c25ce79f91010d1ff073aa6ff3fd01114f90db/packages/istanbul-reports/lib
      reports: [
        'html',
        'text-summary',
        'text',
        'json-summary',
      ],
      fixWebpackSourcePaths: true,
    },

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: [...(debug ? [] : ['ChromeHeadless', 'FirefoxHeadless', 'Electron'])],

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: !debug,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,
  };
};
